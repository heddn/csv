<?php
declare(strict_types=1);

namespace Soong\Csv\Loader;

use Soong\Contracts\Data\RecordPayload;
use Soong\Loader\LoaderBase;

/**
 * Real dumb demo of a simple loader.
 *
 * @todo: Use league/csv
 */
class CsvLoader extends LoaderBase
{

    /**
     * @internal
     *
     * Counter of records loaded, to use as a unique destination ID.
     *
     * @var int $counter
     */
    protected $counter = 0;

    /**
     * @inheritdoc
     */
    public function __invoke(RecordPayload $payload) : RecordPayload
    {
        $destinationRecord = $payload->getDestinationRecord();
        // @todo Should have a configuration option for this.
        $destinationRecord->setPropertyValue(
            array_keys($this->getConfigurationValue('key_properties'))[0],
            $this->counter++
        );
        $properties = $destinationRecord->toArray();
        if (count($properties) > 1) {
            if ($this->counter == 1) {
                print implode(',', $this->getConfigurationValue('properties')) . "\n";
            }
            print implode(',', $properties) . "\n";
        }
        $payload->setDestinationRecord($destinationRecord);
        return $payload;
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        // @todo not supported
    }
}
