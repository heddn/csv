<?php

namespace Soong\Csv\Tests\Loader;

use Soong\Data\BasicRecordFactory;
use Soong\Data\BasicRecordPayload;
use Soong\Tests\Contracts\Loader\LoaderTestBase;

/**
 * Tests the \Soong\Loader\Csv class.
 */
class CsvLoaderTest extends LoaderTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->loaderClass = '\Soong\Csv\Loader\CsvLoader';
    }

    /**
     * Basic test data.
     *
     * @return array
     */
    private function data() : array
    {
        return [
            [
                'positive integer' => 11,
                'negative integer' => -23,
                'numeric string' => '563',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'a string',
                'float' => 1.2345,
                'null' => null,
                'id' => null,
            ],
        ];
    }

    /**
     * Key property configuration.
     *
     * @return array
     */
    private function keyProperties() : array
    {
        return [
            'id' => [
                'type' => 'integer',
            ]
        ];
    }

    /**
     * Basic extractor configuration for testing.
     *
     * @return array
     */
    private function configuration() : array
    {
        return [
            'key_properties' => $this->keyProperties(),
            'properties' => array_keys($this->data()[0]),
        ];
    }

    /**
     * Test load().
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param array $data
     *   Data to load.
     */
    public function testLoad()
    {
        /** @var \Soong\Contracts\Loader\Loader $loader */
        $loader = new $this->loaderClass($this->configuration());
        $expected = implode(',', array_keys($this->data()[0])) . "\n";
        $counter = 0;
        // @todo Mock this.
        $recordFactory = new BasicRecordFactory();
        foreach ($this->data() as $dataArray) {
            $expected .= implode(',', $dataArray) . $counter++ . "\n";
            $record = $recordFactory->create($dataArray);
            // @todo Mock this.
            $payload = new BasicRecordPayload($record, $record);
            $loader($payload);
        }
        $this->expectOutputString($expected);
    }

    /**
     * Test retrieval of property metadata.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        $properties = array_keys($this->data()[0]);
        return [
            'set one' => [$this->configuration(), $properties, $this->keyProperties()],
        ];
    }
}
